# What
The simplest combination of libfaketime and non-superimposing cronjobs, in a Docker container.

The cron job is scheduled every 10s, but using flock the expected behavior is that the script, lasting 60s, blocks and the output is every 60s.

# How
```
FAKETIME_ARG="-20d" docker-compose up --build
```

The output should be a date 20 days before the actual date.
