
FROM debian:buster AS base


RUN set -ex;         \
    apt-get update;  

ADD ./myscript.sh /myscript.sh




from base as builder

# faketime
RUN apt-get update && apt-get install -y make gcc git 
# Get the sources and checkout at stable release 0.98
# see https://github.com/wolfcw/libfaketime/releases
RUN git clone https://github.com/wolfcw/libfaketime.git && \
    cd libfaketime && \
    git checkout dc2ae5eef31c7a64ce3a976487d8e57d50b8d594 && \
    make


from base as runtime

COPY --from=builder /libfaketime/src/libfaketime.so.1 /usr/local/lib 
ENV LD_PRELOAD=/usr/local/lib/libfaketime.so.1
ARG FAKETIME_ARG="0d"
ENV FAKETIME=$FAKETIME_ARG

COPY ./crontab_for_container /etc/cron.d/crontab_for_container 
RUN echo sed --in-place -e "s/FAKETIME_PLACEHOLDER/"$FAKETIME_ARG"/" /etc/cron.d/crontab_for_container
RUN sed --in-place -e s/FAKETIME_PLACEHOLDER/"\"$FAKETIME_ARG\""/ /etc/cron.d/crontab_for_container
RUN apt install -y cron
RUN chmod 0644 /etc/cron.d/crontab_for_container 
RUN crontab /etc/cron.d/crontab_for_container 
RUN touch /var/log/cron.log
CMD cron && tail -f /var/log/cron.log

